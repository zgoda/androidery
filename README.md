# Androidery

Python wrapper over ADB and some support functions to simplify communicating with devices running Android OS. Intentionally uses standalone ADB executable to allow simultaneous use of uiautomator libraries that depend on free access to host USB (Python ADB implementation exclusively locks USB device by way of libusb1).

## Compatibility

Intentionally made it compatible only with Python 3, at least 3.5. Python 3.5.2 is installed on Ubuntu 16.04 LTS and this should be considered minimal Ubuntu version to run this software.

And POSIX only (will not work on Windows).

## Status

Usable and almost complete.

## Licensing

This library is dual-licensed under both GPLv3 and MIT. Choose your poison.
