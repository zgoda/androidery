import os
import io

from setuptools import setup, find_packages


NAME = 'androidery'


here = os.path.abspath(os.path.dirname(__file__))

with io.open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = '\n' + f.read()

about = {}

with open(os.path.join(here, NAME, '__version__.py')) as f:
    exec(f.read(), about)


setup(
    name=NAME,
    version=about['__version__'],
    description='Python library to simplify communication with Android devices',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Jarek Zgoda',
    author_email='jarek.zgoda@gmail.com',
    license='MIT',
    url='https://bitbucket.org/zgoda/androidery',
    packages=find_packages(exclude=['docs', 'tests']),
    install_requires=[
        'iso8601',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Environment :: No Input/Output (Daemon)',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Quality Assurance',
        'Topic :: Software Development :: Testing',
        'Topic :: Software Development :: Libraries'
    ],
    zip_safe=False,
    python_requires='~=3.5'
)
