from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBStopAppTests(ADBTests):

    def setUp(self):
        super(ADBStopAppTests, self).setUp()
        self.app_package = 'org.zgodowie.testapp.application'
        self.missing_package = 'org.zgodowie.otherapp.application'
        self.output = []
        self.failure = [
            'ignored',
        ]

    @mock.patch('subprocess.run')
    def test_stopapp_adb_ok_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        result = self.adb.stop_app(self.app_package)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_stopapp_adb_notrunning_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.stop_app(self.app_package)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_stopapp_adb_outdated_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        result = self.adb.stop_app(self.app_package)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_stopapp_adb_failed_installed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.stop_app, self.app_package)

    @mock.patch('subprocess.run')
    def test_stopapp_adb_ok_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        result = self.adb.stop_app(self.missing_package)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_stopapp_adb_notrunning_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.stop_app(self.missing_package)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_stopapp_adb_outdated_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        result = self.adb.stop_app(self.missing_package)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_stopapp_adb_failed_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.stop_app, self.missing_package)
