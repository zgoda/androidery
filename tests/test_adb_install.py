from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBInstallTests(ADBTests):

    def setUp(self):
        super(ADBInstallTests, self).setUp()
        self.apk_path = 'test.apk'
        self.output = [
            '4317 KB/s (5066221 bytes in 1.145s)',
            '\tpkg: /data/local/tmp/{}'.format(self.apk_path),
            'Success',
        ]

    @mock.patch('subprocess.run')
    def test_install_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        installed = self.adb.install_app(self.apk_path)
        self.assertTrue(installed)

    @mock.patch('subprocess.run')
    def test_install_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        installed = self.adb.install_app(self.apk_path)
        self.assertTrue(installed)

    @mock.patch('subprocess.run')
    def test_install_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        installed = self.adb.install_app(self.apk_path)
        self.assertTrue(installed)

    @mock.patch('subprocess.run')
    def test_install_adb_failure(self, mock_run):
        lines = [
            'ignored',
        ]
        mock_run.return_value = self._mock_failure(lines)
        self.assertRaises(ADBInterfaceError, self.adb.install_app, self.apk_path)
