from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBExecuteScriptTests(ADBTests):

    def setUp(self):
        super(ADBExecuteScriptTests, self).setUp()
        self.failure = [
            'ignored',
        ]
        self.directory = '/sdcard/'
        self.cmd = 'ls {}'.format(self.directory)
        self.output = [
            'Alarms',
            'Android',
            'DCIM',
            'Download',
            'Movies',
            'Music',
            'Notifications'
            'Pictures',
            'Playlists',
            'Podcasts',
            'Ringtones',
        ]

    @mock.patch('subprocess.run')
    def test_executescript_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        result = self.adb.execute_script(self.cmd)
        self.assertEqual(len(result), len(self.output))

    @mock.patch('subprocess.run')
    def test_executescript_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.execute_script(self.cmd)
        self.assertEqual(len(result), len(self.output))

    @mock.patch('subprocess.run')
    def test_executescript_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        result = self.adb.execute_script(self.cmd)
        self.assertEqual(len(result), len(self.output))

    @mock.patch('subprocess.run')
    def test_executescript_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.execute_script, self.cmd)
