from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBListContentTests(ADBTests):

    def setUp(self):
        super(ADBListContentTests, self).setUp()
        self.failure = [
            'ignored',
        ]
        self.directory = '/sdcard/'
        self.output = [
            'Alarms',
            'Android',
            'DCIM',
            'Download',
            'Movies',
            'Music',
            'Notifications'
            'Pictures',
            'Playlists',
            'Podcasts',
            'Ringtones',
        ]
        self.enoent_directory = '/testdir/'
        self.enoent_output = [
            '%s: No such file or directory' % self.enoent_directory,
        ]
        self.eperm_directory = '/root/'
        self.eperm_output = [
            'opendir failed, Permission denied'
        ]

    @mock.patch('subprocess.run')
    def test_listcontent_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        result = self.adb.list_content(self.directory)
        self.assertEqual(len(result), len(self.output))

    @mock.patch('subprocess.run')
    def test_listcontent_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.list_content(self.directory)
        self.assertEqual(len(result), len(self.output))

    @mock.patch('subprocess.run')
    def test_listcontent_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        result = self.adb.list_content(self.directory)
        self.assertEqual(len(result), len(self.output))

    @mock.patch('subprocess.run')
    def test_listcontent_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.list_content, self.directory)

    @mock.patch('subprocess.run')
    def test_listcontent_adb_ok_enoent(self, mock_run):
        mock_run.return_value = self._mock_success(self.enoent_output)
        self.assertRaises(FileNotFoundError, self.adb.list_content, self.enoent_directory)

    @mock.patch('subprocess.run')
    def test_listcontent_adb_notrunning_enoent(self, mock_run):
        mock_run.return_value = self._mock_success(self.enoent_output, condition=self.CONDITION_NOTRUNNING)
        self.assertRaises(FileNotFoundError, self.adb.list_content, self.enoent_directory)

    @mock.patch('subprocess.run')
    def test_listcontent_adb_outdated_enoent(self, mock_run):
        mock_run.return_value = self._mock_success(self.enoent_output, condition=self.CONDITION_OUTDATED)
        self.assertRaises(FileNotFoundError, self.adb.list_content, self.enoent_directory)

    @mock.patch('subprocess.run')
    def test_listcontent_adb_failed_enoent(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.list_content, self.enoent_directory)

    @mock.patch('subprocess.run')
    def test_listcontent_adb_ok_eperm(self, mock_run):
        mock_run.return_value = self._mock_success(self.eperm_output)
        self.assertRaises(PermissionError, self.adb.list_content, self.eperm_directory)

    @mock.patch('subprocess.run')
    def test_listcontent_adb_notrunning_eperm(self, mock_run):
        mock_run.return_value = self._mock_success(self.eperm_output, condition=self.CONDITION_NOTRUNNING)
        self.assertRaises(PermissionError, self.adb.list_content, self.eperm_directory)

    @mock.patch('subprocess.run')
    def test_listcontent_adb_outdated_eperm(self, mock_run):
        mock_run.return_value = self._mock_success(self.eperm_output, condition=self.CONDITION_OUTDATED)
        self.assertRaises(PermissionError, self.adb.list_content, self.eperm_directory)

    @mock.patch('subprocess.run')
    def test_listcontent_adb_failed_eperm(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.list_content, self.eperm_directory)
