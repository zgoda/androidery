from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBGetLogcatFmtTests(ADBTests):

    def setUp(self):
        super(ADBGetLogcatFmtTests, self).setUp()
        self.brief_re = r'^.*\((?P<pid>\s*\d+)\)\:.*$'
        self.brief_output = [
            'D/StatusBar.NetworkController( 1390): refreshNwBoosterIndicator - setNWBoosterIndicators(false)',
            'E/Watchdog(  866): !@Sync 220 [06-18 12:00:15.797]',
            'D/MotionRecognitionService(  866): skip setTransmitPower. ',
            'D/KeyguardUpdateMonitor( 1390): handleBatteryUpdate',
        ]
        self.tag_re = r'^(?P<prio>[A-Z]{1})/(?P<tag>\S+):.*$'
        self.tag_output = [
            'D/DataRouter: Before the usb select',
            'D/BluetoothAdapter: STATE_BLE_ON',
            'D/SSRM:n  : SIOP:: AP = 270, PST = 270, CUR = 450, LCD = 15',
            'E/Watchdog: !@Sync 239 [06-18 12:09:45.946]',
        ]
        self.failed = [
            'ignored',
        ]
        self.brief = 'brief'
        self.tag = 'tag'

    @mock.patch('subprocess.run')
    def test_getlogcatfmt_adb_ok_brief(self, mock_run):
        mock_run.return_value = self._mock_success(self.brief_output)
        result = self.adb.get_logcat_fmt(self.brief)
        self.assertEqual(len(result), 4)
        for line in result:
            self.assertRegex(line, self.brief_re)

    @mock.patch('subprocess.run')
    def test_getlogcatfmt_adb_notrunning_brief(self, mock_run):
        mock_run.return_value = self._mock_success(self.brief_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.get_logcat_fmt(self.brief)
        self.assertEqual(len(result), 4)
        for line in result:
            self.assertRegex(line, self.brief_re)

    @mock.patch('subprocess.run')
    def test_getlogcatfmt_adb_outdated_brief(self, mock_run):
        mock_run.return_value = self._mock_success(self.brief_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.get_logcat_fmt(self.brief)
        self.assertEqual(len(result), 4)
        for line in result:
            self.assertRegex(line, self.brief_re)

    @mock.patch('subprocess.run')
    def test_getlogcatfmt_adb_failure_brief(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failed)
        self.assertRaises(ADBInterfaceError, self.adb.get_logcat_fmt, self.brief)

    @mock.patch('subprocess.run')
    def test_getlogcatfmt_adb_ok_tag(self, mock_run):
        mock_run.return_value = self._mock_success(self.tag_output)
        result = self.adb.get_logcat_fmt(self.tag)
        self.assertEqual(len(result), 4)
        for line in result:
            self.assertRegex(line, self.tag_re)

    @mock.patch('subprocess.run')
    def test_getlogcatfmt_adb_notrunning_tag(self, mock_run):
        mock_run.return_value = self._mock_success(self.tag_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.get_logcat_fmt(self.tag)
        self.assertEqual(len(result), 4)
        for line in result:
            self.assertRegex(line, self.tag_re)

    @mock.patch('subprocess.run')
    def test_getlogcatfmt_adb_outdated_tag(self, mock_run):
        mock_run.return_value = self._mock_success(self.tag_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.get_logcat_fmt(self.tag)
        self.assertEqual(len(result), 4)
        for line in result:
            self.assertRegex(line, self.tag_re)

    @mock.patch('subprocess.run')
    def test_getlogcatfmt_adb_failure_tag(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failed)
        self.assertRaises(ADBInterfaceError, self.adb.get_logcat_fmt, self.tag)
