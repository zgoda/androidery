from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBPullFileTests(ADBTests):

    def setUp(self):
        super(ADBPullFileTests, self).setUp()
        self.filename = 'data.dat'
        self.dest = '/sdcard/data.dat'
        self.wrong_dest = '/data.dat'
        self.failure = [
            'ignored',
        ]
        self.ok_output = [
            '2 KB/s (132 bytes in 0.054s)',
        ]
        self.fail_output = [
            "remote object '%s' does not exist" % self.wrong_dest,
        ]

    @mock.patch('subprocess.run')
    def test_pullfile_adb_ok_right(self, mock_run):
        mock_run.return_value = self._mock_success(self.ok_output)
        result = self.adb.pull_file(self.dest, self.filename)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_pullfile_adb_notrunning_right(self, mock_run):
        mock_run.return_value = self._mock_success(self.ok_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.pull_file(self.dest, self.filename)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_pullfile_adb_outdated_right(self, mock_run):
        mock_run.return_value = self._mock_success(self.ok_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.pull_file(self.dest, self.filename)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_pullfile_adb_failure_right(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.pull_file, self.dest, self.filename)

    @mock.patch('subprocess.run')
    def test_pullfile_adb_ok_wrong(self, mock_run):
        mock_run.return_value = self._mock_success(self.fail_output)
        result = self.adb.pull_file(self.wrong_dest, self.filename)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_pullfile_adb_notrunning_wrong(self, mock_run):
        mock_run.return_value = self._mock_success(self.fail_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.pull_file(self.wrong_dest, self.filename)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_pullfile_adb_outdated_wrong(self, mock_run):
        mock_run.return_value = self._mock_success(self.fail_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.pull_file(self.wrong_dest, self.filename)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_pullfile_adb_failure_wrong(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.pull_file, self.wrong_dest, self.filename)
