from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBClearLogcatTests(ADBTests):

    def setUp(self):
        super(ADBClearLogcatTests, self).setUp()
        self.output = []
        self.failure = [
            'ignored',
        ]

    @mock.patch('subprocess.run')
    def test_clearlogcat_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        result = self.adb.clear_logcat()
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_clearlogcat_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.clear_logcat()
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_clearlogcat_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        result = self.adb.clear_logcat()
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_clearlogcat_adb_failed_installed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.clear_logcat)
