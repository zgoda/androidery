from unittest import mock

from androidery.adbi import ADBInterfaceError
from androidery.logcat import Logcat

from . import ADBTests


class ADBGetLogTests(ADBTests):

    def setUp(self):
        super(ADBGetLogTests, self).setUp()
        self.failure = [
            'ignored',
        ]
        self.output = [
            '--------- beginning of main',
            '06-18 12:21:03.424 D/BatteryService(  866): Sending ACTION_BATTERY_CHANGED.',
            '06-18 12:21:03.424 I/MotionRecognitionService(  866): Plugged',
            '06-18 12:21:03.424 D/MotionRecognitionService(  866):   cableConnection= 1',
            '06-18 12:21:03.424 D/MotionRecognitionService(  866): skip setTransmitPower. ',
            '06-18 12:21:03.434 D/KeyguardUpdateMonitor( 1390): received broadcast android.intent.action.BATTERY_CHANGED',  # noqa
            '06-18 12:21:03.434 I/PERF    ( 1390): received broadcast android.intent.action.BATTERY_CHANGED',
        ]

    @mock.patch('subprocess.run')
    def test_getlog_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        result = self.adb.get_log()
        self.assertIsInstance(result, Logcat)
        self.assertEqual(result.size(), len(self.output) - 1)

    @mock.patch('subprocess.run')
    def test_getlog_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.get_log()
        self.assertIsInstance(result, Logcat)
        self.assertEqual(result.size(), len(self.output) - 1)

    @mock.patch('subprocess.run')
    def test_getlog_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        result = self.adb.get_log()
        self.assertIsInstance(result, Logcat)
        self.assertEqual(result.size(), len(self.output) - 1)

    @mock.patch('subprocess.run')
    def test_getlog_adb_failure(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.get_log)
