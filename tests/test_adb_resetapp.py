from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBResetAppTests(ADBTests):

    def setUp(self):
        super(ADBResetAppTests, self).setUp()
        self.app_package = 'org.zgodowie.testapp.application'
        self.missing_package = 'org.zgodowie.otherapp.application'
        self.success_lines = [
            'Success',
        ]
        self.failure_lines = [
            'ignored',
        ]
        self.missing = [
            'Failed',
        ]

    @mock.patch('subprocess.run')
    def test_reset_adb_ok_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.success_lines)
        result = self.adb.reset_app(self.app_package)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_reset_adb_ok_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.missing)
        result = self.adb.reset_app(self.missing_package)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_reset_adb_notrunning_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.success_lines, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.reset_app(self.app_package)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_reset_adb_notrunning_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.missing, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.reset_app(self.missing_package)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_reset_adb_outdated_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.success_lines, condition=self.CONDITION_OUTDATED)
        result = self.adb.reset_app(self.app_package)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_reset_adb_outdated_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.missing, condition=self.CONDITION_OUTDATED)
        result = self.adb.reset_app(self.missing_package)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_reset_adb_failed_installed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure_lines)
        self.assertRaises(ADBInterfaceError, self.adb.reset_app, self.app_package)

    @mock.patch('subprocess.run')
    def test_reset_adb_failed_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure_lines)
        self.assertRaises(ADBInterfaceError, self.adb.reset_app, self.missing_package)
