from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBLaunchAppTests(ADBTests):

    def setUp(self):
        super(ADBLaunchAppTests, self).setUp()
        self.app_package = 'org.zgodowie.testapp.application'
        self.app_activity = '.activity.main.WelcomeActivity'
        self.missing_package = 'org.zgodowie.otherapp.application'
        self.missing_activity = '.activity.main.StartActivity'
        self.monkey_success_output = [
            'Events injected: 1',
            '## Network stats: elapsed time=54ms (0ms mobile, 0ms wifi, 54ms not connected)',
        ]
        self.monkey_missing_output = [
            '',
            '** No activities found to run, monkey aborted.',
        ]
        self.am_success_output = [
            'Starting: Intent { cmp=%s/%s }' % (self.app_package, self.app_activity),
        ]
        self.am_alreadyrunning_output = [
            'Starting: Intent { cmp=%s/%s }' % (self.app_package, self.app_activity),
            'Warning: Activity not started, its current task has been brought to the front',
        ]
        self.am_missing_output = [
            'Starting: Intent { cmp=%s/%s }' % (self.missing_package, self.missing_activity),
            'Error type 3',
            'Error: Activity class {%s/%s} does not exist.' % (self.missing_package, self.missing_activity),
        ]
        self.failure_output = [
            'ignored',
        ]

    @mock.patch('subprocess.run')
    def test_launch_adb_ok_monkey_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.monkey_success_output)
        result = self.adb.launch_app(self.app_package)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_outdated_monkey_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.monkey_success_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.launch_app(self.app_package)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_notrunning_monkey_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.monkey_success_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.launch_app(self.app_package)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_failed_monkey_installed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure_output)
        self.assertRaises(ADBInterfaceError, self.adb.launch_app, self.app_package)

    @mock.patch('subprocess.run')
    def test_launch_adb_ok_monkey_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.monkey_missing_output)
        result = self.adb.launch_app(self.missing_package)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_outdated_monkey_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.monkey_missing_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.launch_app(self.missing_package)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_notrunning_monkey_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.monkey_missing_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.launch_app(self.missing_package)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_failed_monkey_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure_output)
        self.assertRaises(ADBInterfaceError, self.adb.launch_app, self.missing_package)

    @mock.patch('subprocess.run')
    def test_launch_adb_ok_am_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.am_success_output)
        result = self.adb.launch_app(self.app_package, self.app_activity)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_outdated_am_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.am_success_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.launch_app(self.app_package, self.app_activity)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_notrunning_am_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.am_success_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.launch_app(self.app_package, self.app_activity)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_failed_am_installed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure_output)
        self.assertRaises(ADBInterfaceError, self.adb.launch_app, self.app_package, self.app_activity)

    @mock.patch('subprocess.run')
    def test_launch_adb_ok_am_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.am_missing_output)
        result = self.adb.launch_app(self.missing_package, self.missing_activity)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_outdated_am_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.am_missing_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.launch_app(self.missing_package, self.missing_activity)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_notrunning_am_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.am_missing_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.launch_app(self.missing_package, self.missing_activity)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_launch_adb_failed_am_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure_output)
        self.assertRaises(ADBInterfaceError, self.adb.launch_app, self.missing_package, self.missing_activity)
