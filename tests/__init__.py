import unittest
from unittest import mock
from subprocess import CalledProcessError

from androidery.adbi import ADBInterface


class ADBTests(unittest.TestCase):

    CONDITION_NOTRUNNING = 'notrunning'
    CONDITION_OUTDATED = 'outdated'

    def setUp(self):
        self.adb = ADBInterface()
        self.extra_adb_notrunning = [
            '* daemon not running. starting it now on port 5037 *',
            '* daemon started successfully *',
        ]
        self.extra_adb_outdated = [
            'adb server is out of date.  killing...',
            '* daemon started successfully *',
        ]
        self.extra_lines = {
            self.CONDITION_NOTRUNNING: self.extra_adb_notrunning,
            self.CONDITION_OUTDATED: self.extra_adb_outdated,
        }

    def _mock_success(self, lines, condition=None):
        if condition is None:
            extra = []
        else:
            extra = self.extra_lines.get(condition, [])[:]
        extra.extend(lines)
        mk = mock.Mock()
        mk.stdout = '\n'.join(extra).encode('utf-8')
        mk.check_returncode.return_value = 0
        return mk

    def _mock_failure(self, lines, condition=None):
        if condition is None:
            extra = []
        else:
            extra = self.extra_lines.get(condition, [])[:]
        extra.extend(lines)
        mk = mock.Mock()
        mk.stdout = '\n'.join(extra).encode('utf-8')
        mk.check_returncode.return_value = 1
        mk.check_returncode.side_effect = mock.Mock(
            side_effect=CalledProcessError(1, 'adb devices', output='error')
        )
        return mk
