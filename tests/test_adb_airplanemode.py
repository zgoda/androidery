from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBAirplaneModeTests(ADBTests):

    def setUp(self):
        super().setUp()
        self.lines = [
            'Broadcasting: Intent { act=android.intent.action.AIRPLANE_MODE }',
            'Broadcast completed: result=0'
        ]
        self.error = [
            'ignored'
        ]

    @mock.patch('subprocess.run')
    def test_airplanemode_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.lines)
        result = self.adb.set_airplane_mode(True)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_airplanemode_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.lines, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.set_airplane_mode(True)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_airplanemode_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.lines, condition=self.CONDITION_OUTDATED)
        result = self.adb.set_airplane_mode(True)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_airplanemode_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.error)
        self.assertRaises(ADBInterfaceError, self.adb.set_airplane_mode, True)
