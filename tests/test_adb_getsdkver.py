from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBSdkVerTests(ADBTests):

    def setUp(self):
        super().setUp()
        self.out = [
            '23',
        ]
        self.expected = int(self.out[0])
        self.wrong = [
            'not and integer',
        ]
        self.error = [
            'ignored',
        ]

    @mock.patch('subprocess.run')
    def test_getsdkver_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.out)
        ver = self.adb.get_sdk_version()
        self.assertEqual(ver, self.expected)

    @mock.patch('subprocess.run')
    def test_getsdkver_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.out, condition=self.CONDITION_NOTRUNNING)
        ver = self.adb.get_sdk_version()
        self.assertEqual(ver, self.expected)

    @mock.patch('subprocess.run')
    def test_getsdkver_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.out, condition=self.CONDITION_OUTDATED)
        ver = self.adb.get_sdk_version()
        self.assertEqual(ver, self.expected)

    @mock.patch('subprocess.run')
    def test_getsdkver_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.error)
        self.assertRaises(ADBInterfaceError, self.adb.get_sdk_version)

    @mock.patch('subprocess.run')
    def test_getsdkver_wrong_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong)
        self.assertRaises(ValueError, self.adb.get_sdk_version)

    @mock.patch('subprocess.run')
    def test_getsdkver_wrong_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong, condition=self.CONDITION_NOTRUNNING)
        self.assertRaises(ValueError, self.adb.get_sdk_version)

    @mock.patch('subprocess.run')
    def test_getsdkver_wrong_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong, condition=self.CONDITION_OUTDATED)
        self.assertRaises(ValueError, self.adb.get_sdk_version)
