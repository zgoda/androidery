from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBUninstallTests(ADBTests):

    def setUp(self):
        super(ADBUninstallTests, self).setUp()
        self.app_package = 'org.zgodowie.testapp.application'
        self.missing_package = 'org.zgodowie.otherapp.application'
        self.success_lines = [
            'Success',
        ]
        self.failure_lines = [
            'ignored',
        ]
        self.missing = [
            'Failure [DELETE_FAILED_INTERNAL_ERROR]',
        ]

    @mock.patch('subprocess.run')
    def test_unistall_adb_ok_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.success_lines)
        result = self.adb.uninstall_app(self.app_package)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_unistall_adb_notrunning_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.success_lines, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.uninstall_app(self.app_package)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_uninstall_adb_outdated_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.success_lines, condition=self.CONDITION_OUTDATED)
        result = self.adb.uninstall_app(self.app_package)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_uninstall_adb_failure_installed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure_lines)
        self.assertRaises(ADBInterfaceError, self.adb.uninstall_app, self.app_package)

    @mock.patch('subprocess.run')
    def test_unistall_adb_ok_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.missing)
        result = self.adb.uninstall_app(self.missing_package)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_unistall_adb_notrunning_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.missing, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.uninstall_app(self.missing_package)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_uninstall_adb_outdated_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_success(self.missing, condition=self.CONDITION_OUTDATED)
        result = self.adb.uninstall_app(self.missing_package)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_uninstall_adb_failure_notinstalled(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure_lines)
        self.assertRaises(ADBInterfaceError, self.adb.uninstall_app, self.missing)
