from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBIsAppInstalledTests(ADBTests):

    def setUp(self):
        super(ADBIsAppInstalledTests, self).setUp()
        self.app_package = 'org.zgodowie.testapp.application'
        self.missing_package = 'org.zgodowie.otherapp.application'
        self.output = [
            '',
            'package:{}'.format(self.app_package),
        ]

    @mock.patch('subprocess.run')
    def test_isappinstalled_adb_ok_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        installed = self.adb.is_app_installed(self.app_package)
        self.assertTrue(installed)

    @mock.patch('subprocess.run')
    def test_isappinstalled_adb_ok_notinstalled(self, mock_run):
        lines = []
        mock_run.return_value = self._mock_success(lines)
        installed = self.adb.is_app_installed(self.missing_package)
        self.assertFalse(installed)

    @mock.patch('subprocess.run')
    def test_isappinstalled_adb_failure_installed(self, mock_run):
        lines = [
            'ignored',
        ]
        mock_run.return_value = self._mock_failure(lines)
        self.assertRaises(ADBInterfaceError, self.adb.is_app_installed, self.app_package)

    @mock.patch('subprocess.run')
    def test_isappinstalled_adb_failure_notinstalled(self, mock_run):
        lines = [
            'ignored',
        ]
        mock_run.return_value = self._mock_failure(lines)
        self.assertRaises(ADBInterfaceError, self.adb.is_app_installed, self.missing_package)

    @mock.patch('subprocess.run')
    def test_isappinstalled_adb_outdated_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        installed = self.adb.is_app_installed(self.app_package)
        self.assertTrue(installed)

    @mock.patch('subprocess.run')
    def test_isappinstalled_adb_outdated_notinstalled(self, mock_run):
        lines = [
            '',
        ]
        mock_run.return_value = self._mock_success(lines, condition=self.CONDITION_OUTDATED)
        installed = self.adb.is_app_installed(self.missing_package)
        self.assertFalse(installed)

    @mock.patch('subprocess.run')
    def test_isappinstalled_adb_notrunning_installed(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        installed = self.adb.is_app_installed(self.app_package)
        self.assertTrue(installed)

    @mock.patch('subprocess.run')
    def test_isappinstalled_adb_notrunning_notinstalled(self, mock_run):
        lines = [
            '',
        ]
        mock_run.return_value = self._mock_success(lines, condition=self.CONDITION_NOTRUNNING)
        installed = self.adb.is_app_installed(self.missing_package)
        self.assertFalse(installed)
