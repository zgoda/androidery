from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBDevicesTests(ADBTests):

    def setUp(self):
        super(ADBDevicesTests, self).setUp()
        self.output = [
            'List of devices attached ',
            'ddeec0a1	device',
        ]
        self.serial = 'ddeec0a1'

    @mock.patch('subprocess.run')
    def test_devices_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        devices = self.adb.get_devices()
        self.assertIn(self.serial, devices)
        self.assertEqual(len(devices), 1)

    @mock.patch('subprocess.run')
    def test_devices_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        devices = self.adb.get_devices()
        self.assertIn(self.serial, devices)
        self.assertEqual(len(devices), 1)

    @mock.patch('subprocess.run')
    def test_devices_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        devices = self.adb.get_devices()
        self.assertIn(self.serial, devices)
        self.assertEqual(len(devices), 1)

    @mock.patch('subprocess.run')
    def test_devices_adb_failure(self, mock_run):
        lines = [
            'irrelevant',
        ]
        mock_run.return_value = self._mock_failure(lines)
        self.assertRaises(ADBInterfaceError, self.adb.get_devices)
