from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBGetLogcatTests(ADBTests):

    def setUp(self):
        super(ADBGetLogcatTests, self).setUp()
        self.output = [
            '--------- beginning of main',
            '06-18 11:38:04.580  1390  1390 D StatusBar.NetworkController: onSignalStrengthsChanged signalStrength=SignalStrength: 17 99 -120 -160 -120 -1 -1 99 2147483647 2147483647 2147483647 -1 2147483647 0x4 gsm|lte level=4',  # noqa
            '06-18 11:38:04.580  1390  1390 D StatusBar.NetworkController: updateTelephonySignalStrength: hasService=true ss=SignalStrength: 17 99 -120 -160 -120 -1 -1 99 2147483647 2147483647 2147483647 -1 2147483647 0x4 gsm|lte',  # noqa
            '06-18 11:38:04.580  1390  1390 D StatusBar.NetworkController: updateTelephonySignalStrength: iconLevel=4',  # noqa
        ]
        self.failed = [
            'ignored',
        ]

    @mock.patch('subprocess.run')
    def test_getlogcat_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        result = self.adb.get_logcat()
        self.assertEqual(len(result), 4)

    @mock.patch('subprocess.run')
    def test_getlogcat_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.get_logcat()
        self.assertEqual(len(result), 4)

    @mock.patch('subprocess.run')
    def test_getlogcat_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        result = self.adb.get_logcat()
        self.assertEqual(len(result), 4)

    @mock.patch('subprocess.run')
    def test_getlogcat_adb_failure(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failed)
        self.assertRaises(ADBInterfaceError, self.adb.get_logcat)
