from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBDeleteFileTests(ADBTests):

    def setUp(self):
        super(ADBDeleteFileTests, self).setUp()
        self.path = '/sdcard/data.dat'
        self.wrong_path = '/data.dat'
        self.output = []
        self.failure = [
            'ignored'
        ]
        self.error = [
            'rm: %s: Read-only file system' % self.wrong_path,
        ]

    @mock.patch('subprocess.run')
    def test_deletefile_adb_ok_right(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        result = self.adb.delete_file(self.path)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_deletefile_adb_notrunning_right(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.delete_file(self.path)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_deletefile_adb_outdated_right(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        result = self.adb.delete_file(self.path)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_deletefile_adb_failed_right(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.delete_file, self.path)

    @mock.patch('subprocess.run')
    def test_deletefile_adb_ok_wrong(self, mock_run):
        mock_run.return_value = self._mock_success(self.error)
        result = self.adb.delete_file(self.wrong_path)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_deletefile_adb_notrunning_wrong(self, mock_run):
        mock_run.return_value = self._mock_success(self.error, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.delete_file(self.wrong_path)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_deletefile_adb_outdated_wrong(self, mock_run):
        mock_run.return_value = self._mock_success(self.error, condition=self.CONDITION_OUTDATED)
        result = self.adb.delete_file(self.wrong_path)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_deletefile_adb_failed_wrong(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.delete_file, self.wrong_path)
