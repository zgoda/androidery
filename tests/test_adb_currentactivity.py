from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBCurrentActivityTests(ADBTests):

    def setUp(self):
        super(ADBCurrentActivityTests, self).setUp()
        self.package = 'com.sec.android.app.launcher'
        self.activity = 'com.android.launcher2.Launcher'
        self.output = [
            '  mCurrentFocus=Window{9228fc9 u0 d0 %s/%s}' % (self.package, self.activity),
        ]
        self.error = [
            'ignored',
        ]

    @mock.patch('subprocess.run')
    def test_currentactivity_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        package, activity = self.adb.current_activity()
        self.assertEqual(self.package, package)
        self.assertEqual(self.activity, activity)

    @mock.patch('subprocess.run')
    def test_currentactivity_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        package, activity = self.adb.current_activity()
        self.assertEqual(self.package, package)
        self.assertEqual(self.activity, activity)

    @mock.patch('subprocess.run')
    def test_currentactivity_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        package, activity = self.adb.current_activity()
        self.assertEqual(self.package, package)
        self.assertEqual(self.activity, activity)

    @mock.patch('subprocess.run')
    def test_currentactivity_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.error)
        self.assertRaises(ADBInterfaceError, self.adb.current_activity)
