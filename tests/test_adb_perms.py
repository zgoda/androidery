from unittest import mock

from androidery.error import UnknownPackageError, UnknownPermissionError, ADBInterfaceError

from . import ADBTests


class ADBPermsTests(ADBTests):

    def setUp(self):
        super().setUp()
        self.right_perm = 'android.permission.RIGHT'
        self.wrong_perm = 'android.permission.WRONG'
        self.wrong_perm_out = [
            'Bad argument: java.lang.IllegalArgumentException: Unknown permission: %s' % self.wrong_perm,
            'ignored',
        ]
        self.right_pkg = 'org.zgodowie.testap.application'
        self.wrong_pkg = 'org.zgodowie.wrongapp.application'
        self.wrong_pkg_out = [
            'Bad argument: java.lang.IllegalArgumentException: Unknown package: %s' % self.wrong_pkg,
            'ignored',
        ]
        self.error = [
            'ignored'
        ]

    @mock.patch('subprocess.run')
    def test_grant_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success([])
        result = self.adb.grant_permission(self.right_pkg, self.right_perm)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_grant_wrongperm_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_perm_out)
        self.assertRaises(UnknownPermissionError, self.adb.grant_permission, self.right_pkg, self.wrong_perm)

    @mock.patch('subprocess.run')
    def test_grant_wrongpkg_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_pkg_out)
        self.assertRaises(UnknownPackageError, self.adb.grant_permission, self.wrong_pkg, self.right_perm)

    @mock.patch('subprocess.run')
    def test_revoke_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success([])
        result = self.adb.revoke_permission(self.right_pkg, self.right_perm)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_revoke_wrongperm_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_perm_out)
        self.assertRaises(UnknownPermissionError, self.adb.revoke_permission, self.right_pkg, self.wrong_perm)

    @mock.patch('subprocess.run')
    def test_revoke_wrongpkg_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_pkg_out)
        self.assertRaises(UnknownPackageError, self.adb.revoke_permission, self.wrong_pkg, self.right_perm)

    @mock.patch('subprocess.run')
    def test_grant_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success([], condition=self.CONDITION_OUTDATED)
        result = self.adb.grant_permission(self.right_pkg, self.right_perm)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_grant_wrongperm_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_perm_out, condition=self.CONDITION_OUTDATED)
        self.assertRaises(UnknownPermissionError, self.adb.grant_permission, self.right_pkg, self.wrong_perm)

    @mock.patch('subprocess.run')
    def test_grant_wrongpkg_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_pkg_out, condition=self.CONDITION_OUTDATED)
        self.assertRaises(UnknownPackageError, self.adb.grant_permission, self.wrong_pkg, self.right_perm)

    @mock.patch('subprocess.run')
    def test_revoke_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success([], condition=self.CONDITION_OUTDATED)
        result = self.adb.revoke_permission(self.right_pkg, self.right_perm)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_revoke_wrongperm_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_perm_out, condition=self.CONDITION_OUTDATED)
        self.assertRaises(UnknownPermissionError, self.adb.revoke_permission, self.right_pkg, self.wrong_perm)

    @mock.patch('subprocess.run')
    def test_revoke_wrongpkg_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_pkg_out, condition=self.CONDITION_OUTDATED)
        self.assertRaises(UnknownPackageError, self.adb.revoke_permission, self.wrong_pkg, self.right_perm)

    @mock.patch('subprocess.run')
    def test_grant_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success([], condition=self.CONDITION_NOTRUNNING)
        result = self.adb.grant_permission(self.right_pkg, self.right_perm)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_grant_wrongperm_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_perm_out, condition=self.CONDITION_NOTRUNNING)
        self.assertRaises(UnknownPermissionError, self.adb.grant_permission, self.right_pkg, self.wrong_perm)

    @mock.patch('subprocess.run')
    def test_grant_wrongpkg_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_pkg_out, condition=self.CONDITION_NOTRUNNING)
        self.assertRaises(UnknownPackageError, self.adb.grant_permission, self.wrong_pkg, self.right_perm)

    @mock.patch('subprocess.run')
    def test_revoke_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success([], condition=self.CONDITION_NOTRUNNING)
        result = self.adb.revoke_permission(self.right_pkg, self.right_perm)
        self.assertIsNone(result)

    @mock.patch('subprocess.run')
    def test_revoke_wrongperm_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_perm_out, condition=self.CONDITION_NOTRUNNING)
        self.assertRaises(UnknownPermissionError, self.adb.revoke_permission, self.right_pkg, self.wrong_perm)

    @mock.patch('subprocess.run')
    def test_revoke_wrongpkg_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.wrong_pkg_out, condition=self.CONDITION_NOTRUNNING)
        self.assertRaises(UnknownPackageError, self.adb.revoke_permission, self.wrong_pkg, self.right_perm)

    @mock.patch('subprocess.run')
    def test_grant_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.error)
        self.assertRaises(ADBInterfaceError, self.adb.grant_permission, self.right_pkg, self.right_perm)

    @mock.patch('subprocess.run')
    def test_grant_wrongperm_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.error)
        self.assertRaises(ADBInterfaceError, self.adb.grant_permission, self.right_pkg, self.wrong_perm)

    @mock.patch('subprocess.run')
    def test_grant_wrongpkg_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.error)
        self.assertRaises(ADBInterfaceError, self.adb.grant_permission, self.wrong_pkg, self.right_perm)

    @mock.patch('subprocess.run')
    def test_revoke_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.error)
        self.assertRaises(ADBInterfaceError, self.adb.revoke_permission, self.right_pkg, self.right_perm)

    @mock.patch('subprocess.run')
    def test_revoke_wrongperm_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.error)
        self.assertRaises(ADBInterfaceError, self.adb.revoke_permission, self.right_pkg, self.wrong_perm)

    @mock.patch('subprocess.run')
    def test_revoke_wrongpkg_adb_failed(self, mock_run):
        mock_run.return_value = self._mock_failure(self.error)
        self.assertRaises(ADBInterfaceError, self.adb.revoke_permission, self.wrong_pkg, self.right_perm)
