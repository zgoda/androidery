from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBFileExistsTests(ADBTests):

    def setUp(self):
        super(ADBFileExistsTests, self).setUp()
        self.failure = [
            'ignored',
        ]
        self.filename = '/sdcard/data.dat'
        self.output = [
            self.filename,
        ]
        self.enoent_filename = '/sdcard/missing.dat'
        self.enoent_output = [
            '%s: No such file or directory' % self.enoent_filename,
        ]
        self.eperm_filename = '/root/data.dat'
        self.eperm_output = [
            '%s: Permission denied' % self.eperm_filename,
        ]

    @mock.patch('subprocess.run')
    def test_fileexists_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        result = self.adb.file_exists(self.filename)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.file_exists(self.filename)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        result = self.adb.file_exists(self.filename)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_faled(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.file_exists, self.filename)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_ok_enoent(self, mock_run):
        mock_run.return_value = self._mock_success(self.enoent_output)
        result = self.adb.file_exists(self.enoent_filename)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_notrunning_enoent(self, mock_run):
        mock_run.return_value = self._mock_success(self.enoent_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.file_exists(self.enoent_filename)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_outdated_enoent(self, mock_run):
        mock_run.return_value = self._mock_success(self.enoent_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.file_exists(self.enoent_filename)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_faled_enoent(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.file_exists, self.enoent_filename)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_ok_eperm(self, mock_run):
        mock_run.return_value = self._mock_success(self.eperm_output)
        result = self.adb.file_exists(self.eperm_filename)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_notrunning_eperm(self, mock_run):
        mock_run.return_value = self._mock_success(self.eperm_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.file_exists(self.eperm_filename)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_outdated_eperm(self, mock_run):
        mock_run.return_value = self._mock_success(self.eperm_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.file_exists(self.eperm_filename)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_fileexists_adb_faled_eperm(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.file_exists, self.eperm_filename)
