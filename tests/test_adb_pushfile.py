from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBPushFileTests(ADBTests):

    def setUp(self):
        super(ADBPushFileTests, self).setUp()
        self.filename = 'data.dat'
        self.dest = '/sdcard/data.dat'
        self.wrong_dest = '/data.dat'
        self.failure = [
            'ignored',
        ]
        self.ok_output = [
            '2 KB/s (132 bytes in 0.054s)',
        ]
        self.fail_output = [
            "failed to copy '%s' to '%s': Read-only file system" % (self.filename, self.wrong_dest),
        ]

    @mock.patch('subprocess.run')
    def test_pushfile_adb_ok_right(self, mock_run):
        mock_run.return_value = self._mock_success(self.ok_output)
        result = self.adb.push_file(self.filename, self.dest)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_pushfile_adb_notrunning_right(self, mock_run):
        mock_run.return_value = self._mock_success(self.ok_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.push_file(self.filename, self.dest)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_pushfile_adb_outdated_right(self, mock_run):
        mock_run.return_value = self._mock_success(self.ok_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.push_file(self.filename, self.dest)
        self.assertTrue(result)

    @mock.patch('subprocess.run')
    def test_pushfile_adb_failure_right(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.push_file, self.filename, self.dest)

    @mock.patch('subprocess.run')
    def test_pushfile_adb_ok_wrong(self, mock_run):
        mock_run.return_value = self._mock_success(self.fail_output)
        result = self.adb.push_file(self.filename, self.wrong_dest)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_pushfile_adb_notrunning_wrong(self, mock_run):
        mock_run.return_value = self._mock_success(self.fail_output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.push_file(self.filename, self.wrong_dest)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_pushfile_adb_outdated_wrong(self, mock_run):
        mock_run.return_value = self._mock_success(self.fail_output, condition=self.CONDITION_OUTDATED)
        result = self.adb.push_file(self.filename, self.wrong_dest)
        self.assertFalse(result)

    @mock.patch('subprocess.run')
    def test_pushfile_adb_failure_wrong(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failure)
        self.assertRaises(ADBInterfaceError, self.adb.push_file, self.filename, self.wrong_dest)
