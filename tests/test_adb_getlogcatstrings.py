from unittest import mock

from androidery.adbi import ADBInterfaceError

from . import ADBTests


class ADBGetLogcatStringsTests(ADBTests):

    def setUp(self):
        super(ADBGetLogcatStringsTests, self).setUp()
        self.output = [
            'Setting tx/rx TCP buffers to 4096,87380,110208,4096,16384,110208',
            'cleanUpApplicationRecord -- 13029',
            'isAutoRunBlockedApp:: com.sec.android.app.videoplayer, Auto Run ON',
            'isWidgetUsingPkg : com.sec.android.app.videoplayer no widget is using.'
        ]
        self.failed = [
            'ignored',
        ]

    @mock.patch('subprocess.run')
    def test_getlogcatstrings_adb_ok(self, mock_run):
        mock_run.return_value = self._mock_success(self.output)
        result = self.adb.get_logcat_strings()
        self.assertEqual(len(result), 4)

    @mock.patch('subprocess.run')
    def test_getlogcatstrings_adb_notrunning(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_NOTRUNNING)
        result = self.adb.get_logcat_strings()
        self.assertEqual(len(result), 4)

    @mock.patch('subprocess.run')
    def test_getlogcatstrings_adb_outdated(self, mock_run):
        mock_run.return_value = self._mock_success(self.output, condition=self.CONDITION_OUTDATED)
        result = self.adb.get_logcat_strings()
        self.assertEqual(len(result), 4)

    @mock.patch('subprocess.run')
    def test_getlogcatstrings_adb_failure(self, mock_run):
        mock_run.return_value = self._mock_failure(self.failed)
        self.assertRaises(ADBInterfaceError, self.adb.get_logcat_strings)
