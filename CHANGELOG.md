# Changelog

2018-06-23 v1.3

* SDK version retrieval
* grant/revoke permission

2018-06-22 v1.2

* device airplane mode control (up to Android 6)

2018-06-18 v1.1

* complete test suite for `ADBInterface`;
* return boolean indication of outcome whenever `adb` returns anything meaningful

2018-06-15 v1.0

* initial release
