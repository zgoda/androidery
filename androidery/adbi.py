import subprocess
import shlex

from .logcat import Logcat
from .util import PropParser
from .error import ADBInterfaceError, UnknownPackageError, UnknownPermissionError


class ADBInterface(object):
    """Thin wrapper over adb executable."""

    _ADB_SERVICE_MESSAGES = [
        'daemon not running',
        'adb server is out of date',
        'daemon started successfully',
    ]

    def __init__(self, serial=None, adb_host=None, adb_port=None):
        self.serial = serial
        self.adb_host = adb_host
        self.adb_port = adb_port
        self.last_result = None

    def setup_adb(self, serial=None, adb_host=None, adb_port=None):
        if serial is not None:
            self.serial = serial
        if adb_host is not None:
            self.adb_host = adb_host
        if adb_port is not None:
            self.adb_port = adb_port

    def adb_command(self, command, serial=None, capture=True):
        self.last_result = []
        adb_command = ['adb']
        serial = serial or self.serial
        if serial is not None:
            adb_command.extend(['-s', serial])
        if self.adb_host is not None:
            adb_command.extend(['-H', self.adb_host])
        if self.adb_port is not None:
            adb_command.extend(['-P', str(self.adb_port)])
        adb_command.extend(shlex.split(command))
        kw = dict(stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        if capture:
            kw['stdout'] = subprocess.PIPE
        cp = subprocess.run(adb_command, **kw)
        try:
            cp.check_returncode()
            if capture and cp.stdout is not None:
                stdout = cp.stdout.decode('utf-8')
                lines = []
                for line in stdout.strip().split('\n'):
                    line = line.strip()
                    if not any([m for m in self._ADB_SERVICE_MESSAGES if m in line]) and line:
                        lines.append(line)
                self.last_result = lines
                return lines
        except subprocess.CalledProcessError as e:
            raise ADBInterfaceError('adb return code: %s: %s' % (e.returncode, e.output))

    def get_devices(self):
        """List connected devices.

        :returns: list of serial numbers of all connected devices"""
        out = [line.strip() for line in self.adb_command('devices') if line.strip()]
        if len(out) == 1:
            return []
        out = out[1:]
        devices = []
        for line in out:
            if '\t' in line:
                serial, name = line.split('\t')
                devices.append(serial)
        return devices

    def get_sdk_version(self, serial=None):
        """Get specific device SDK version as numeric value. Raises ValueError on non-numeric version.

        :returns: int"""
        lines = self.execute_script('getprop ro.build.version.sdk')
        return int(lines[0].strip())

    def grant_permission(self, package, permission, serial=None):
        out = self.execute_script('pm grant {} {}'.format(package, permission), serial=serial)
        if len(out) != 0:
            msg = out[0].lower()
            if 'unknown permission' in msg:
                raise UnknownPermissionError(permission)
            elif 'unknown package' in msg:
                raise UnknownPackageError(package)

    def revoke_permission(self, package, permission, serial=None):
        out = self.execute_script('pm revoke {} {}'.format(package, permission), serial=serial)
        if len(out) != 0:
            msg = out[0].lower()
            if 'unknown permission' in msg:
                raise UnknownPermissionError(permission)
            elif 'unknown package' in msg:
                raise UnknownPackageError(package)

    def get_build_properties(self, serial=None):
        """Read and return complete system build properties from device as dictionary. All values
        are returned as strings.

        :returns: dictionary of system build properties from device"""
        props = self.execute_script('cat system/build.prop')
        parser = PropParser(strict=False)
        parser.read_string('\n'.join(props))
        return parser.get_props()

    def is_app_installed(self, package, serial=None):
        """Check if package is installed on device."""
        packages = self.adb_command('shell pm list packages %s' % package, serial=serial)
        if len(packages) == 0:
            return False
        for line in packages:
            if package in line:
                return True
        return False

    def install_app(self, apk_path, serial=None):
        """Install package on device using local file.

        :returns: True if application has been installed, false otherwise"""
        output = self.adb_command('install -r %s' % apk_path, serial=serial)
        for line in output:
            if 'success' in line.lower():
                return True
        return False

    def uninstall_app(self, package, serial=None):
        """Uninstall package from device.

        :returns: True if package has been uninstalled, False otherwise"""
        output = self.adb_command('uninstall %s' % package, serial=serial)
        for line in output:
            if 'success' in line.lower():
                return True
        return False

    def reset_app(self, package, serial=None):
        """Reset package data. Returns True if application data has been reset."""
        out = self.adb_command('shell pm clear %s' % package, serial=serial)
        return 'success' in [m.lower() for m in out]

    def launch_app(self, package, activity=None, serial=None):
        """Launch specified activity from package, or default launcher activity if no specific
        activity is provided.

        Returns True if application has been started, False otherwise."""
        if activity is None:
            cmd = 'shell monkey -p %s 1' % package
        else:
            cmd = 'shell am start -n %s/%s' % (package, activity)
        out = self.adb_command(cmd, serial=serial)
        errors = (
            'No activities found to run',  # from monkey
            'Error type 3',  # from activity manager
        )
        for line in out:
            if any([m for m in errors if m in line]):
                return False
        return True

    def stop_app(self, package, serial=None):
        """Force stop any activity from package."""
        self.adb_command('shell am force-stop %s' % package, serial=serial, capture=False)

    def get_logcat(self, clear=True, serial=None):
        """Get log as list of raw strings in default format and optionally clear logcat on device.
        This changes in different versions of ADB, in 2018 it's 'threadtime'."""
        log = self.adb_command('logcat -d', serial=serial)
        if clear:
            self.adb_command('logcat -c', serial=serial, capture=False)
        return log

    def get_logcat_strings(self, clear=True, serial=None):
        """Get log as raw message strings, without any entry metadata. Optionally logcat may be cleared."""
        log = self.adb_command('logcat -d -v raw', serial=serial)
        if clear:
            self.adb_command('logcat -c', serial=serial, capture=False)
        return log

    def get_logcat_fmt(self, fmt, clear=True, serial=None):
        """Get log as list of raw strings in specified format and optionally clear logcat on device.
        Format is expected as one of supported by adb logcat, most common set is "brief", "color",
        "long", "printable", "process", "raw", "tag", "thread", "threadtime", "time", "usec".

        Reference: https://developer.android.com/studio/command-line/logcat#outputFormat
        """
        log = self.adb_command('logcat -d -v %s' % fmt, serial=serial)
        if clear:
            self.adb_command('logcat -c', serial=serial, capture=False)
        return log

    def clear_logcat(self, serial=None):
        """Clear logcat on device."""
        self.adb_command('logcat -c', serial=serial, capture=False)

    def get_log(self, fmt='time', clear=True, serial=None):
        """Get content of device log in structured format."""
        lines = self.get_logcat_fmt(fmt, clear, serial)
        log = Logcat(fmt)
        log.feed(lines)
        return log

    def push_file(self, filename, dest, serial=None):
        """Push local file to specified destination on device."""
        out = self.adb_command('push %s %s' % (filename, dest), serial=serial)
        for line in out:
            if 'failed' in line:
                return False
        return True

    def pull_file(self, file_path, dest, serial=None):
        """Download file from device to local destination. Returns False upon failure, True otherwise."""
        out = self.adb_command('pull %s %s' % (file_path, dest), serial=serial)
        if len(out) > 0 and 'does not exist' in out[0]:
            return False
        return True

    def delete_file(self, file_path, serial=None):
        """Delete file on device."""
        self.adb_command('shell rm -rf %s' % file_path, serial=serial, capture=False)

    def list_content(self, remote_path, serial=None):
        """Return content of remote location. Raises standard Python exceptions corresponding
        to ENOENT and EPERM."""
        out = self.adb_command('shell ls %s' % remote_path, serial=serial)
        for line in out:
            x = line.lower()
            if 'no such file' in x:
                raise FileNotFoundError(line)
            elif 'permission denied' in x:
                raise PermissionError(line)
        return out

    def file_exists(self, file_path, serial=None):
        """Check if specified file exists on device."""
        try:
            out = self.list_content(file_path, serial)
        except (FileNotFoundError, PermissionError):
            return False
        return len(out) == 1

    def execute_script(self, command, serial=None):
        """Execute arbitrary adb shell command and return its output."""
        return self.adb_command('shell %s' % command, serial=serial)

    def current_activity(self):
        """Return current package and activity as tuple."""
        line = self.execute_script('dumpsys window windows | grep -E mCurrentFocus')
        k, v = line[0].split('=')
        rest, activity = v.split('/')
        package = rest.split()[-1]
        return (package, activity[:-1])

    def airplane_mode_on(self, serial=None):
        return self.set_airplane_mode(True, serial=serial)

    def airplane_mode_off(self, serial=None):
        return self.set_airplane_mode(False, serial=serial)

    def set_airplane_mode(self, value, serial=None):
        if value:
            mode = '1'
        else:
            mode = '0'
        self.execute_script('settings put global airplane_mode_on {}'.format(mode), serial=serial)
        out = self.execute_script('am broadcast -a android.intent.action.AIRPLANE_MODE', serial=serial)
        for line in out:
            if 'result=0' in line:
                return True
        return False
