import configparser


class PropParser(configparser.RawConfigParser):

    NOSECTION = 'NOSECTION'

    def read_string(self, string, source='<string>'):
        data = '[{}]\n{}'.format(self.NOSECTION, string)
        super(PropParser, self).read_string(data, source=source)

    def get_option(self, option, default=None):
        return self.get(self.NOSECTION, option, fallback=default)

    def get_options_list(self):
        return self.options(self.NOSECTION)

    def has_option(self, option):
        return super(PropParser, self).has_option(self.NOSECTION, option)

    def get_props(self):
        data = self.items(self.NOSECTION)
        return dict(data)
