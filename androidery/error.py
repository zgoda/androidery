class AndroideryException(Exception):
    pass


class ADBInterfaceError(AndroideryException):
    pass


class OperationalError(AndroideryException):
    pass


class UnknownPermissionError(OperationalError):
    pass


class UnknownPackageError(OperationalError):
    pass
