import re
from datetime import datetime

from iso8601 import parse_date


class Logcat(object):

    """Logcat message container for specified format. Currently supported are `brief` and `time`,
    and they are equivalent to standard logcat format specifiers."""

    __RE_FLAGS = re.VERBOSE | re.MULTILINE | re.DOTALL
    _BRIEF_RE = re.compile(
        r"""^
            (?P<level>[VDIWEFS])
            /
            (?P<tag>.*)
            \(
            (?P<pid>\s+\d+)
            \):\s+
            (?P<content>.*)
            $""",
        __RE_FLAGS
    )
    _TIME_RE = re.compile(
        r"""^
            (?P<time>\d{2}-\d{2}\s{1}\d{2}:\d{2}:\d{2}\.\d{3})
            \s
            (?P<level>[VDIWEFS])
            /
            (?P<tag>.*)
            \(
            (?P<pid>\s+\d+)
            \):\s+
            (?P<content>.*)
            $""",
        __RE_FLAGS
    )
    _LOGCAT_RE = {
        'brief': _BRIEF_RE,
        'time': _TIME_RE,
    }
    _LOG_LEVEL = {
        'V': 'Verbose',
        'D': 'Debug',
        'I': 'Info',
        'W': 'Warning',
        'E': 'Error',
        'F': 'Fatal',
        'S': 'Silent',
    }

    def __init__(self, fmt):
        self.format = fmt
        self.re = self._LOGCAT_RE[fmt]
        self._entries = []
        self._by_tag = {}
        now = datetime.now()
        self._year = now.year

    def _normalize_time(self, data):
        if self.format != 'time':
            return
        item_time = data.get('time')
        if item_time is not None:
            item_time = '%d-%s' % (self._year, item_time)
            dt = parse_date(item_time, default_timezone=None)
            data['time'] = dt

    def size(self):
        return len(self._entries)

    def feed(self, lines):
        """Fill object with logcat entries.

        :param lines: iterable providing log entries, one line per entry"""
        for line in lines:
            match = self.re.match(line)
            if match:
                data = match.groupdict()
                self._normalize_time(data)
                self._entries.append(data)
                tag = data.get('tag')
                if tag is not None:
                    tag = tag.strip()
                    entries = self._by_tag.setdefault(tag, [])
                    entries.append(data)

    def find(self, text, first_only=False, tag=None, level=None):
        """Find entries containing text, optionally limiting search to tag and message level. Text
        search is case insensitive.

        If specified tag does not exist, all entries will be searched. Tag specification is case
        sensitive.

        Level may be specified by either logcat abbreviation or full level name as in logcat command
        reference, see https://developer.android.com/studio/command-line/logcat. Level specification is
        case insensitive.

        Log entries are returned as list of dictionary items sorted as original unless first_only is
        specified.

        :param text: text to search entries for
        :param first_only: whether to return 1st (oldest) occurence of searched text (default is False)
        :param tag: limit search to entries with specified tag, all tags if None
        :param level: filter entries by priority level, either as in logcat (single letter) or full name,
                      all levels if None
        :returns: either list of log entries or single entry if `first_only` has been requested
        """
        found = []
        if tag is not None:
            dataset = self._by_tag.get(tag, self._entries)
        else:
            dataset = self._entries
        for item in dataset:
            conditions = [text.lower() in item['content'].lower()]
            if level is not None:
                if len(level) > 1:
                    level = level[0]
                conditions.append(item['level'].upper() == level.upper())
            if all(conditions):
                if first_only:
                    return item
                found.append(item)
        return found

    def find_first(self, text, tag=None, level=None):
        """Shortcut method to return 1st occurence of text in log, optionally limiting search to tag and
        message level.

        :param text: text to search entries for
        :param tag: limit search to entries with specified tag, all tags if None
        :param level: filter entries by priority level, either as in logcat (single letter) or full name,
                      all levels if None
        :returns: single entry (dictionary)
        """
        return self.find(text, first_only=True, tag=tag, level=level)
